<?php
require_once ('connec_db.php');


function getAuthentification($pdo,$login,$pass){
  $query = "SELECT * FROM Compte where name=:login and password=:pass";
  $prep = $pdo->prepare($query);
  $prep->bindValue(':login', $login);
  $prep->bindValue(':pass', $pass);
  $prep->execute()
;
  // on vérifie que la requête ne retourne qu'une seule ligne
  if($prep->rowCount() == 1){
    $result = $prep->fetch(PDO::FETCH_ASSOC);
    return $result;
  }
 else
  return false;
}

function getDM($id)
{
  global $pdo;
     $query = "SELECT message.text FROM message, compte_has_message, compte WHERE message.idmessage=compte_has_message.message_idmessage
                                                                 AND compte_has_message.compte_idtable1=compte.idtable1
                                                                 AND compte.idtable1=:id";
     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetchAll();
}

function getMessageForum($id){
  global $pdo;
     $query = "SELECT message.text FROM message, forum_has_message, forum WHERE message.idmessage=forum_has_message.message_idmessage
                                                                 AND forum_has_message.forum_idforum=forum.idforum
                                                                 AND forum.idforum=:id";
     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetchAll();
}

function getNameForum($id){
  global $pdo;
     $query = "SELECT * FROM forum WHERE forum.forum_father_idforum_father=:id";

     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetchAll();
}

function getSonforum($id){
  global $pdo;
     $query = "SELECT * FROM forum F1 WHERE NOT EXISTS
                                                (SELECT * FROM forum F2 WHERE F2.forum_father_idforum_father=F1.idforum)
                                                AND EXISTS (select * FROM forum F3 WHERE F1.forum_father_idforum_father=F3.idforum AND F3.idforum=:id)";
     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetchAll();

}

function getUserMessageName($id){
  global $pdo;
     $query = "SELECT compte.name FROM compte,message,forum_has_message,forum WHERE compte.idtable1=message.fk_compte AND message.idmessage=forum_has_message.message_idmessage
                                                                           AND forum_has_message.forum_idforum=forum.idforum
                                                                           AND forum.idforum=:id";
     $prep = $pdo->prepare($query);
     $prep->bindValue(':id',$id, PDO::PARAM_INT);
     $prep->execute();
     return $prep->fetchAll();
}








?>
